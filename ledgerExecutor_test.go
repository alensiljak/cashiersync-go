// Tests for Ledger Executor module.
package main

import (
	"os/exec"
	"strings"
	"testing"
)

func TestAccounts(t *testing.T) {
	result, err := runLedger("accounts")

	// Don't attempt to handle ExitErrors. This is an error reported by Ledger,
	// like not having a ledger file, etc.
	if err, ok := err.(*exec.ExitError); !ok && (err != nil) {
		t.Error(err)
	}
	if result == "" {
		t.Error("The result is empty!")
	}
}

func TestMultipleParameters(t *testing.T) {
	params := []string{"b", "--flat", "--no-total"}
	result, err := runLedger(params...)

	if err, ok := err.(*exec.ExitError); !ok && (err != nil) {
		t.Error(err)
	}
	if result == "" {
		t.Error("The result is empty!")
	}
	if strings.HasPrefix(result, "Error") {
		t.Error(result)
	}
}

func TestRetrieveCurrentValues(t *testing.T) {
	root := "Assets:Invest"
	currency := "EUR"

	params := []string{"b", "^" + root, "-X", currency, "--flat", "--no-total"}
	result, err := runLedger(params...)

	if err, ok := err.(*exec.ExitError); !ok && (err != nil) {
		t.Error(err)
	}
	if result == "" {
		t.Error("The result is empty!")
	}
	if strings.HasPrefix(result, "Error") {
		t.Error(result)
	}
}
