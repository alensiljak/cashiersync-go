// helpers for tests
package main

import (
	"io"
	"net/http"
	"net/http/httptest"
)

func fetchGet(url string) (*httptest.ResponseRecorder, error) {
	return fetch("GET", url, nil)
}

func fetchPost(url string, body io.Reader) (*httptest.ResponseRecorder, error) {
	return fetch("POST", url, body)
}

func fetch(method string, url string, body io.Reader) (*httptest.ResponseRecorder, error) {
	r := setupRouter()
	w := httptest.NewRecorder()

	req, err := http.NewRequest(method, url, body)
	r.ServeHTTP(w, req)

	return w, err
}
