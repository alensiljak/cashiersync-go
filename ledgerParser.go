// Ledger output parser
package main

import "strings"

/// Returns total line from the ledger output.
func get_total_lines(lines []string) []string {
	// Extract the total lines from the output,
	// unless there is only one account, in which case use the complete output

	var result []string
	var totalLine string
	nextLineIsTotal := false

	if len(lines) == 1 {
		// No income is an array with an empty string ['']
		if lines[0] == "" {
			totalLine = "0"
		} else {
			// One-line results don't have totals
			totalLine = lines[0]
		}
		result = append(result, totalLine)
	} else {
		for i := range lines {
			if nextLineIsTotal {
				totalLine = lines[i]
				result = append(result, totalLine)
			} else {
				if strings.Contains(lines[i], "------") {
					nextLineIsTotal = true
				}
			}
		}
	}

	if totalLine == "" {
		panic("No total received!")
	}

	return result
}

// Removes the empty valuse from a string split array.
// Used mostly when separating by double-space '  ', to clean up.
func remove_blank_values_from_splits(parts []string) []string {
	var result []string

	for _, part := range parts {
		if part != "" {
			result = append(result, part)
		}
	}

	return result
}

func trim_all(parts []string) []string {
	for i := range parts {
		parts[i] = strings.TrimSpace(parts[i])
	}
	return parts
}
