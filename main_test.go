// Gin tests
package main

import (
	"encoding/json"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Test /currentValues
// This is an example of how to use Gin requests.
// Ref: https://stackoverflow.com/questions/59186562/unit-testing-with-gin-gonic
func TestCurrentValues(t *testing.T) {
	url := "/currentValues?root=Assets:Invest&currency=EUR"
	w, err := fetchGet(url)

	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)

	//assert.Equal(t, "pong", w.Body.String())
	body := w.Body.String()
	assert.NotEmpty(t, body)
}

func TestLots(t *testing.T) {
	url := "/lots?symbol=VEUR"
	w, err := fetchGet(url)

	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)

	body := w.Body.String()
	assert.NotEmpty(t, body)
}

func TestEmptyLots(t *testing.T) {
	url := "/lots?symbol=BASF"
	w, err := fetchGet(url)

	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)

	body := w.Body.String()
	assert.Equal(t, "null", body)
}

func TestSecurityDetails(t *testing.T) {
	url := "/securitydetails?symbol=VHY_AX&currency=EUR"
	w, err := fetchGet(url)

	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)

	body := w.Body.String()
	assert.NotEmpty(t, body)
}

func TestSecurityDetails2(t *testing.T) {
	url := "/securitydetails?symbol=VEUR&currency=EUR"
	w, err := fetchGet(url)

	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)

	body := w.Body.String()
	assert.NotEmpty(t, body)
}

// Providing a non-existing symbol should return a valid value.
func TestSecurityDetailsEmpty(t *testing.T) {
	url := "/securitydetails?symbol=VEXX&currency=EUR"
	w, err := fetchGet(url)

	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)

	body := w.Body.String()
	assert.NotEmpty(t, body)
}

func TestTransactions(t *testing.T) {
	url := "/transactions?account=gratis&dateFrom=2021-03-01&dateTo=2021-04-01"
	w, err := fetchGet(url)

	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)

	body := w.Body.String()
	assert.NotEmpty(t, body)
}

// Test xact
func TestXact(t *testing.T) {
	url := "/xact?date=2022-01-08&payee=alen&freeText=yo"
	w, err := fetchGet(url)

	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)

	body := w.Body.String()
	assert.NotEmpty(t, body)
}

func TestXactMissingParam(t *testing.T) {
	url := "/xact"
	load := strings.NewReader("date=2022-01-08\npayee=alen")
	w, err := fetchPost(url, load)

	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)

	body := w.Body.String()
	assert.NotEmpty(t, body)
}

func TestSearch(t *testing.T) {
	url := "/search"
	params := &SearchParams{DateFrom: "2021-10-10", DateTo: "2021-12-31"}
	paramsJson, err := json.Marshal(params)
	if err != nil {
		panic(err)
	}
	paramsString := string(paramsJson)
	reader := strings.NewReader(paramsString)

	w, err := fetchPost(url, reader)

	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)

	body := w.Body.String()
	assert.NotEmpty(t, body)
}
