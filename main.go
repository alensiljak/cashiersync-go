package main

import (
	"bufio"
	"strings"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type SearchParams struct {
	DateFrom string
	DateTo   string
	Payee    string
	FreeText string
}

func setupRouter() *gin.Engine {
	// Setting up Gin
	r := gin.Default()

	// CORS
	r.Use(cors.Default())

	r.GET("/", hello) // home
	r.GET("/hello", hello_img)
	r.GET("/accounts", accounts)
	r.GET("/balance", balance)
	r.GET("/currentValues", currentValues)
	r.GET("/lots", lots)
	r.GET("/payees", payees)
	r.POST("/search", search)
	r.GET("/securitydetails", securityDetails)
	r.GET("/transactions", transactions)
	r.POST("/xact", xact)
	r.GET("/about", about)
	r.GET("/shutdown", shutdown)

	return r
}

func main() {
	// define routes
	r := setupRouter()

	// todo: make the port configurable
	r.Run("localhost:8080") // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

// The Go way to split the string at newline characters.
func SplitLines(s string) []string {
	var lines []string
	sc := bufio.NewScanner(strings.NewReader(s))
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	return lines
}
