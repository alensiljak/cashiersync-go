// ledger transactions
package main

import "strings"

// Fetch historical transactions and return an array
func get_transactions(account string, dateFrom string, dateTo string) []Transaction {
	params := []string{"r", account, "-b", dateFrom, "-e", dateTo}
	output, err := runLedger(params...)
	if err != nil {
		panic(output)
	}

	lines := SplitLines(output)

	// parse
	var txs []Transaction
	for _, line := range lines {
		tx := get_tx_from_line(line)
		// dict
		txs = append(txs, tx)
	}
	return txs
}

type Transaction struct {
	Date    string
	Payee   string
	Account string
	Amount  string
	Total   string
}

// parse ledger output line and create a Transaction object
func get_tx_from_line(line string) Transaction {
	parts := strings.Split(line, "  ")
	parts = remove_blank_values_from_splits(parts)
	parts = trim_all(parts)

	// date, payee, account, amount, total

	date_payee := strings.SplitN(parts[0], " ", 2)

	tx := Transaction{}
	tx.Date = date_payee[0]
	tx.Payee = date_payee[1]
	tx.Account = parts[1]
	tx.Account = parts[2]
	tx.Total = parts[3]

	return tx
}
