//package controllers
package main

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
)

func hello(c *gin.Context) {
	c.String(http.StatusOK, "Hello World!")
}

func accounts(c *gin.Context) {
	cmd := "accounts"
	result, err := runLedger(cmd)

	if err != nil {
		log.Fatal(err)
	}

	// split lines
	lines := SplitLines(result)

	c.JSON(http.StatusOK, lines)
}

func balance(c *gin.Context) {
	cmd := []string{"b", "--flat", "--no-total"}
	result, err := runLedger(cmd...)

	if err != nil {
		log.Fatal(err)
	}

	c.JSON(http.StatusOK, result)
}

func currentValues(c *gin.Context) {
	// get parameters
	root := c.Query("root")
	if root == "" {
		panic("root parameter not sent!")
	}
	currency := c.Query("currency")
	if currency == "" {
		panic("currency parameter not sent!")
	}

	params := []string{"b", "^" + root, "-X", currency, "--flat", "--no-total"}
	result, err := runLedger(params...)

	if err != nil {
		log.Fatal(err)
	}

	parsed := make(map[string]string)
	// parse the Ledger output
	lines := strings.Split(result, "\n")
	for _, row := range lines {
		// strip
		row = strings.TrimSpace(row)
		// ignore empty rows
		if row == "" {
			continue
		}
		// split at the root account name
		root_index := strings.Index(row, root)
		//parts := strings.Split(row, root)
		amount := row[:root_index]
		amount = strings.TrimSpace(amount)
		account := row[root_index:]

		//fmt.Println(amount, account)
		// todo: add to an output map
		parsed[account] = amount
	}

	c.JSON(http.StatusOK, parsed)
}

func lots(c *gin.Context) {
	symbol := c.Query("symbol")
	lots, err := getLots(symbol)
	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, lots)
}

func payees(c *gin.Context) {
	cmd := "payees"
	result, err := runLedger(cmd)

	if err != nil {
		log.Fatal(err)
	}

	//c.String(http.StatusOK, result)
	c.JSON(http.StatusOK, result)
}

func search(c *gin.Context) {
	dateFrom := c.Query("dateFrom")

	c.JSON(http.StatusOK, dateFrom)
}

// Displays the security details (analysis)
func securityDetails(c *gin.Context) {
	symbol := c.Query("symbol")
	currency := c.Query("currency")

	//sec_details = SecurityDetails(logger, symbol, currency)
	//result = sec_details.calculate()
	secDetails := calculateSecurityDetails(symbol, currency)

	c.JSON(http.StatusOK, secDetails)
}

func transactions(c *gin.Context) {
	account := c.Query("account")
	dateFrom := c.Query("dateFrom")
	dateTo := c.Query("dateTo")

	result := get_transactions(account, dateFrom, dateTo)

	// workaround for empty output.
	// var interfaceSlice []interface{} = make([]interface{}, len(result))
	// for i, item := range result {
	// 	interfaceSlice[i] = item
	// }

	c.JSON(http.StatusOK, result)
}

func xact(c *gin.Context) {
	dateParam := c.Query("date")
	payeeParam := c.Query("payee")
	freeTextParam := c.Query("freeText")

	params := "xact "

	if dateParam != "" {
		params += dateParam + " "
	}
	if payeeParam != "" {
		params += "@" + payeeParam
	}
	if freeTextParam != "" {
		params += freeTextParam
	}

	output, err := runLedger(params)
	if err != nil {
		panic(err)
	}

	c.String(http.StatusOK, output)
}

// Returns an image.
// Call with a random parameter so it is not cached on the client-side.
func hello_img(c *gin.Context) {
	// Base64 encoded pixel
	pixelEncoded := "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg=="

	sDec, err := base64.StdEncoding.DecodeString(pixelEncoded)
	if err != nil {
		msg := fmt.Sprintf("Error decoding string: %s ", err.Error())
		fmt.Println(msg)
		//return nil
		panic(msg)
	}

	byteReader := bytes.NewReader(sDec)
	var length int64 = int64(byteReader.Len())

	c.DataFromReader(http.StatusOK, length, "image/png", byteReader, nil)
}

func about(c *gin.Context) {
	c.String(http.StatusOK, "CashierSync Go")
}

func shutdown(c *gin.Context) {
	os.Exit(1)
}
